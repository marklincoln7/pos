//this is just a placeholder for the real login page

#include "MainWindow.h"
#include "TableWindow.h"
#include "ui_MainWindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
//    connect(ui->loginButton, SIGNAL(clicked()), this, SLOT(employeeLogin()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

//void MainWindow::employeeLogin(){
//    TableWindow table;
//    table.show();
//}

void MainWindow::on_loginButton_clicked()
{
    QString id = ui->empID->text();
    int idNum = id.toInt(0, 10);
    employeeSelect(idNum);
    this->hide();
}

void MainWindow::showLogin(){
    ui->empID->setText("");
    this->show();
}
