//written by David De Leon
#ifndef ORDERTICKET_H
#define ORDERTICKET_H

#include <string>
#include <vector>
#include "Employee.h"
#include "MenuItem.h"

typedef std::vector< std::vector<MenuItem> > MIvect;

class OrderTicket
{
private:
    int empNum, tableSeatNum, item_id, itemQuantity;
    std::string item_desc;
    //the above variables are redundant with the emp and order variables
    int order_id, tableNum;
    double order_Time_In;
    //this vector translates to customer<MenuItem<quantity>>>
//    std::vector< std::vector< std::vector<MenuItem> > > order;
    MIvect order;
    Employee emp;


public:
/* constructors */
    OrderTicket();
    ~OrderTicket();
/* mutators */
    void setOrderID(int);
    //Replace empNum with an employee object?
    void setEmpNum(int);
    void setEmp(Employee);
    void setTableNum(int);
    void setSeatNum(int);
    /*we may want to set item_id to a MenuItem, and have a Vector<MenuItem> to
     * allow for many items, maybe even a Vector< Vector<MenuItem> > to make
     * seperated customer/tickets, maybe an aditional dimension for quantity
     *  or we may want to create a new object that would contain the MenuItem
     *  along with which customer it is for and the quantity. This would also let
     *  us remove the item_desc*/
    void setItemId(int);
    void setQuantity(int cust, int itemID, int quant);
    void setItemDesc(std::string);
    void setOrderTime(double);
/* accessors */
    int getOrderId();
    int getEmpNum();
    Employee getEmp();
    int getTableNum();
    int getSeatNum();
    int getItemId(int cust, int orderIndex);
    int getQuantity(int cust, int indexID);
    std::string getItemDesc();
    double getOrderTime();
/* other member functions */
    void addItemToOrder(int cust, MenuItem item);
    void removeItem(int cust, int itemId);
    double calculateOrder();
    double calculateOrder(int cust);
    MIvect debugOrderTicket();
    MenuItem getItemAt(int cust, int itemIndex);
    void cleanOrderTicket();
};

#endif // ORDERTICKET_H
