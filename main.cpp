#include "OrderForm.h"
#include "TableWindow.h"
#include <QApplication>
#include "mainwindow.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
      OrderForm order;
//    order.show();
//    order.hide();
      TableWindow tables;
      MainWindow window;

//      tables.show();
//    MainWindow window;
    window.show();

    //GENERIC CONNECTION TEMPLATE
    //QObject::connect(&CALLING_OBJECT,SIGNAL(CALLING_FUNCTION(ARGS)), &RECIEVING_OBJECT, SLOT(RECIEVING_FUNCTION(ARGS)));
    //SIGNAL clicking a table button will open the order page for that table
    QObject::connect(&tables, SIGNAL(tableSelect(int, int)),&order, SLOT(setTable(int, int)));
//PLACEHOLDER SIGNAL mainwindow activates tablewindow
    QObject::connect(&window, SIGNAL(employeeSelect(int)),&tables, SLOT(setEmployee(int)));
//PLACEHOLDER SIGNAL connects multiple order page buttons back to the inital login page
    QObject::connect(&order,SIGNAL(endSession()), &window, SLOT(showLogin()));
    return a.exec();
}
