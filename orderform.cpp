//written by David De Leon
#include "OrderForm.h"
#include "OrderTicket.h"
#include "MenuItem.h"
#include "ui_OrderForm.h"
#include <string>
#include <QListWidget>
#include <QListWidgetItem>
//#include <QtTest/QTestEvent>
#include <QKeyEvent>
#include <cstdlib>
#include <ctime>
#include <QDebug>


OrderForm::OrderForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::OrderForm)
{
    TAX = .06;
    ui->setupUi(this);
}

OrderForm::~OrderForm()
{
    delete ui;
}

/************************************************************************
 * Public slots
 ************************************************************************/

void OrderForm::setTable(int table, int emp){
    setEmpNum(emp);
    setTableNum(table);
    showOrderPage();

}

/************************************************************************
 * Private slots
 ************************************************************************/

void OrderForm::on_AddItemButton_clicked()
{
//    ui->listWidget;
    QString item = ui->MenuItemEdit->text();
    int idNum = item.toInt();
    if(item.isEmpty())
        return;
//TODO validate MenuItem
//workaround - the price is pulled from the name only when the name is int or double
    double price = item.toDouble();
    MenuItem menuItem;
    menuItem.setItemPrice(price);
    //QString id = ui->MenuItemEdit->text();

    //add the item to the GUI
    int quantity = ui->quantSpin->value();
    item += ", x" + QString::number(quantity);
    int customer = ui->custSpin->value();
    item += ", cust #" + QString::number(customer);
    new QListWidgetItem(item, ui->listWidget, 0);
    //add the item to the ticket object
    menuItem.setItemId(idNum);
    menuItem.setItemPrice(price);
    ticket.addItemToOrder(customer, menuItem);
    //recalculate order
    double tot = ticket.calculateOrder();
    //segfault
    //MenuItem myItem = ticket.getItemAt(0, 0);
//    QString debugtext = "price of item = " + QString::number(myItem.getItemPrice());
//    ui->debuglineEdit->setText(debugtext);
    calculateTotals(tot);

}

void OrderForm::on_RemoveItemButton_clicked()
{
//TODO crashes when there is no item selected
    int row = ui->listWidget->currentRow();
    //if there is no item selected, stop
    if(row==-1)
        return;
    QListWidgetItem * rem = ui->listWidget->takeItem(row);
    //remove the item from the ticket
    QString item = rem->text();
    QStringList itemList = item.split(",");
    item = itemList.at(0);
    int itemId = item.toInt(0, 10);
//    std::string itemString = item.toStdString();
//    int endSubStr = itemString.find_first_of(",");
//    std::string itemIdStr = itemString.substr(0, endSubStr);
////TODO: maybe change the following to switch from itemID to item description
//    std::string::size_type sz;
    //int itemId = std::stoi(itemIdStr, &sz);
    item = itemList.at(2).split("#").at(1);
    int cust = item.toInt(0, 10);
    ticket.removeItem(cust, itemId);

    //recalculate order
    calculateTotals(ticket.calculateOrder());
}

void OrderForm::on_CashOutButton_clicked()
{
//TODO finalize the order and print recipts
    closeOrder();
}

void OrderForm::on_CancelOrderButton_clicked()
{
//TODO void out order, print a void recipt?
//TODO: delete any temperary files
    closeOrder();
}

void OrderForm::on_SaveOrderButton_clicked()
{
//TOD save the order to a semperary file
    closeOrder();
}

void OrderForm::on_custSelectSpin_valueChanged(int custNum)
{
    double total;
    //this will recalculate the order totals
//     qDebug() << "custNum=" << custNum;
    if(custNum == 0){
        //calculate total of entire bill
        total = ticket.calculateOrder();
//        qDebug() << "cust 0 - total=" << total;
    }
    else{
        //calculate total for selected customer - 1
        total = ticket.calculateOrder(custNum-1);
//        qDebug() << "cust - total=" << total;
        //if customer number does not exist, update totals to zeros
        if (total == -1){
            calculateTotals(0);
//            qDebug() << "total == -1";
            return;
        }
    }
    calculateTotals(total);
}

void OrderForm::on_testButton_clicked()
{
    unsigned seed = time(0);
    srand(seed);
    int numOrders = rand()%10+5, customer = 1;
    for(int i = 0; i < numOrders; i++){
        QString item = QString::number(rand()%10+1);
        ui->MenuItemEdit->setText(item);
        ui->custSpin->setValue(customer);
        on_AddItemButton_clicked();
        if(rand()%100<30){
            customer++;
        }
    }
}

/************************************************************************
 * setters and getters
 ************************************************************************/

int OrderForm::getTableNum(){
    QString numStr = ui->tableNumLabel->text();
    int num = numStr.toInt();
    return num;
}

void OrderForm::setTableNum(int table){
    QString tableStr = QString::number(table);
    ui->tableNumLabel->setText(tableStr);
    ui->tableNumLabel->hide();
}

int OrderForm::getEmpNum(){
    QString numStr = ui->empNumLabel->text();
    int num = numStr.toInt();
    return num;
}

void OrderForm::setEmpNum(int emp){
    QString empStr = QString::number(emp);
    ui->empNumLabel->setText(empStr);
    ui->empNumLabel->hide();
}

/************************************************************************
 * other private methods
 ************************************************************************/

void OrderForm::showOrderPage(){
    int tableNo = getTableNum();
//    printf("the int passed to showOrderPage is %i\n", tableNo);
    QString tabletext = " Table #";
    tabletext += QString::number(tableNo);
//    printf("the new text is %s\n", tabletext.toUtf8().constData());
    ui->TableText->setText(tabletext);
    QString title = "Order for" + tabletext;
    setWindowTitle(title);
    this->show();

}

void OrderForm::calculateTotals(double subtotal){
    double taxed = subtotal * TAX;
    double totaled = subtotal + taxed;
    QString temp = "$" + QString::number(subtotal,'f',2);
    ui->SubtotalText->setText(temp);
    temp = "$" + QString::number(taxed,'f',2);
    ui->TaxText->setText(temp);
    temp = "$" + QString::number(totaled,'f',2);
    ui->GrantTotalText->setText(temp);
}

void OrderForm::closeOrder(){
    clearUI();
    ticket.cleanOrderTicket();
    calculateTotals(0);
    ui->listWidget->clear();
    endSession();
    this->hide();
}

void OrderForm::clearUI(){
    ui->custSelectSpin->setValue(1);
    ui->custSpin->setValue(1);
    ui->quantSpin->setValue(1);
    QString empty = "";
    ui->MenuItemEdit->setText(empty);
}

/************************************************************************
 * protected methods
 ************************************************************************/

bool OrderForm::eventFilter(QObject *obj, QEvent *event)
{
    if(event->type() == QEvent::KeyPress)
    {
        QKeyEvent *key = static_cast<QKeyEvent *>(event);

        if((key->key() == Qt::Key_Enter) || (key->key() == Qt::Key_Return))
        {
            //Enter or return was pressed
            on_AddItemButton_clicked();
        }
        else
        {
            return QObject::eventFilter(obj, event);
        }
        return true;
    }
    else
    {
        return QObject::eventFilter(obj, event);
    }

    return false;
}


//void OrderForm::keyPressEvent(QKeyEvent* pe)
//{
// if(pe->key() == Qt::Key_Return)
//     on_AddItemButton_clicked();
//  else
//     return QObject::eventFilter(obj, event);
//}
